﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public Material MRTMapMaterial;
    public Material KRLMapMaterial;
    public Material LRTMapMaterial;
    public GameObject MapsPlane;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void SwitchToMRTMap()
    {
        MapsPlane.GetComponent<Renderer>().material = MRTMapMaterial;
    }

    public void SwitchToLRTMap()
    {
        MapsPlane.GetComponent<Renderer>().material = LRTMapMaterial;
    }

    public void SwitchToKRLMap()
    {
        MapsPlane.GetComponent<Renderer>().material = KRLMapMaterial;
    }

    public void QuitProgram()
    {
        Application.Quit();
    }
}
