﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationManager : MonoBehaviour
{
    //Variables
    public Text TimeIndicator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TimeIndicator.text = System.DateTime.Now.ToString("dddd, HH:mm:ss");
        //Debug.Log(System.DateTime.Now.ToString("dddd, HH:mm"));
    }
}
